import { execSync } from 'child_process';
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import {
  invokeScript,
  broadcast,
  waitForTx,
  setScript,
} from '@waves/waves-transactions';
import { NODE_URL, STATE, CHAIN_ID } from '../state';
import { getLastBlock, getDayEnd } from '../utils/';
import { DAY_IN_MS, STRIKE, WVS } from '../utils/constants';

describe('dapp optioner', () => {
  describe('withdraw procedure', () => {
    beforeAll(async () => {
      const nl = '\n';
      console.log(
        '===',
        nl,
        "DON'T FORTGET TO COMMENT LINE IN RIDE SCRIPT IN EMIT PROCEDURE WITH EXP DATE CHECKING!",
        nl,
        "IT'S NEED FOR EXECUTION TESTING!",
        nl,
        '==='
      );

      const path = join('.', 'ride', 'optioner.ride');
      const newFilePath = join('.', 'ride', 'optioner.ride.tmp');
      writeFileSync(
        newFilePath,
        readFileSync(path)
          .toString()
          .replace('PLACERADDRESSHERE', STATE.ACCOUNTS.ALICE.address)
      );
      const script = execSync(`surfboard compile ${newFilePath}`)
        .toString()
        .trim();
      execSync(`rm ${newFilePath}`);

      const tx = await broadcast(
        setScript(
          {
            fee: 0.014 * WVS,
            script: script,
            senderPublicKey: STATE.ACCOUNTS.DAPP.publicKey,
            type: 13,
            timestamp: new Date().valueOf(),
            version: 1,
            chainId: CHAIN_ID,
          },
          STATE.ACCOUNTS.DAPP.seed
        ),
        NODE_URL
      );
      await waitForTx(tx.id, {
        apiBase: NODE_URL,
      });
    });

    /**
     * "withdraw" procedure
     * - try to withdraw option by option not-issuer
     * - try to withdraw not expired option (with today expDate)
     * - try to withdraw option with valid data
     * - try to withdraw not-active (already withdrawed) option
     */
    it('should try to withdraw option by option not-issuer', async done => {
      const expDate = new Date().valueOf() - DAY_IN_MS;
      const optionId = STATE.ASSETS.OPT1CALL.id;

      try {
        /**
         * emit option via alice
         * try to withdraw option via bob
         */

        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let withdrawTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'withdraw',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.BOB.seed
          ),
          NODE_URL
        );

        await waitForTx(withdrawTx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The option can be withdrawed only by its issuer'
        );
        done();
      }
    });

    it('should try to withdraw not expired option (with today expDate)', async done => {
      const expDate = new Date().valueOf();
      const optionId = STATE.ASSETS.OPT2CALL.id;

      try {
        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let withdrawTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'withdraw',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(withdrawTx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The option is not expired'
        );
        done();
      }
    });

    it('should try to withdraw option', async done => {
      const expDate = new Date().valueOf() - DAY_IN_MS;
      const optionId = STATE.ASSETS.OPT3CALL.id;

      try {
        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let withdrawTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'withdraw',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(withdrawTx.id, {
          apiBase: NODE_URL,
        });

        done();
      } catch (e) {
        done(e.message);
      }
    });

    it('should try to withdraw not-active (already withdrawed) option', async done => {
      const expDate = new Date().valueOf() - DAY_IN_MS;
      const optionId = STATE.ASSETS.OPT4CALL.id;

      try {
        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let withdrawTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'withdraw',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(withdrawTx.id, {
          apiBase: NODE_URL,
        });

        let withdraw2Tx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'withdraw',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(withdraw2Tx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The option is not active'
        );
        done();
      }
    });
  });
});
