import { execSync } from 'child_process';
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import {
  invokeScript,
  broadcast,
  waitForTx,
  setScript,
} from '@waves/waves-transactions';
import { NODE_URL, STATE, CHAIN_ID } from '../state';
import { DAY_IN_MS, STRIKE, WVS } from '../utils/constants';
import { getLastBlock, getDayEnd } from '../utils/';

describe('dapp optioner', () => {
  describe('execute procedure', () => {
    beforeAll(async () => {
      const nl = '\n';
      console.log(
        '===',
        nl,
        "DON'T FORTGET TO COMMENT LINE IN RIDE SCRIPT IN EMIT PROCEDURE WITH EXP DATE CHECKING!",
        nl,
        "IT'S NEED FOR EXECUTION TESTING!",
        nl,
        '==='
      );

      const path = join('.', 'ride', 'optioner.ride');
      const newFilePath = join('.', 'ride', 'optioner.ride.tmp');
      writeFileSync(
        newFilePath,
        readFileSync(path)
          .toString()
          .replace('PLACERADDRESSHERE', STATE.ACCOUNTS.ALICE.address)
      );
      const script = execSync(`surfboard compile ${newFilePath}`)
        .toString()
        .trim();
      execSync(`rm ${newFilePath}`);

      const tx = await broadcast(
        setScript(
          {
            fee: 0.014 * WVS,
            script: script,
            senderPublicKey: STATE.ACCOUNTS.DAPP.publicKey,
            type: 13,
            timestamp: new Date().valueOf(),
            version: 1,
            chainId: CHAIN_ID,
          },
          STATE.ACCOUNTS.DAPP.seed
        ),
        NODE_URL
      );

      await waitForTx(tx.id, {
        apiBase: NODE_URL,
      });
    });

    /**
     * "execute" procedure
     * - try to execute option with not existing NFT
     * - try to execute option by option not-owner
     * - try to execute option with tomorrow expDate
     * - try to execute option with incorrect payment amount
     * - try to execute option with incorrect payment assetId
     * - try to execute option with valid data
     * - try to execute not-active (already executed) option
     */
    it('should try to execute option with not existing NFT', async done => {
      const nftId = 'NFTSSTD';
      try {
        let executeTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'execute',
                args: [
                  {
                    type: 'string',
                    value: nftId,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(executeTx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The asset does not exists'
        );
        done();
      }
    });

    it('should try to execute option by option not-owner', async done => {
      const expDate = new Date().valueOf() + DAY_IN_MS;
      const optionId = STATE.ASSETS.OPT1CALL.id;

      try {
        /**
         * emit option via alice
         * try to execute option via bob
         */

        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let executeTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'execute',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [
                {
                  assetId: STATE.ASSETS.WUSD.id,
                  amount: STRIKE,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.BOB.seed
          ),
          NODE_URL
        );

        await waitForTx(executeTx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The option can be executed only by its owner'
        );
        done();
      }
    });

    it('should try to execute option with tomorrow expDate', async done => {
      const expDate = new Date().valueOf() + DAY_IN_MS;
      const optionId = STATE.ASSETS.OPT2CALL.id;

      try {
        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let executeTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'execute',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [
                {
                  amount: STRIKE,
                  assetId: STATE.ASSETS.WUSD.id,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(executeTx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The option expiration date has not come'
        );
        done();
      }
    });

    it('should try to execute option with invalid payment amount', async done => {
      const expDate = new Date().valueOf();
      const optionId = STATE.ASSETS.OPT3CALL.id;

      try {
        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let executeTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'execute',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [
                {
                  amount: 1,
                  assetId: STATE.ASSETS.WUSD.id,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(executeTx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The payment amount is not satisfy option execution requirements'
        );
        done();
      }
    });

    it('should try to execute option with invalid payment assetId', async done => {
      const expDate = new Date().valueOf();
      const optionId = STATE.ASSETS.OPT4CALL.id;

      try {
        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let executeTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'execute',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [
                {
                  amount: STRIKE,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(executeTx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The payment assetId is not satisfy option execution requirements'
        );
        done();
      }
    });

    it('should try to execute option with valid data', async done => {
      const expDate = new Date().valueOf();
      const optionId = STATE.ASSETS.OPT5CALL.id;

      try {
        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let executeTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'execute',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [
                {
                  amount: STRIKE,
                  assetId: STATE.ASSETS.WUSD.id,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(executeTx.id, {
          apiBase: NODE_URL,
        });

        done();
      } catch (e) {
        done(e.message);
      }
    });

    it('should try to execute not-active (already executed) option', async done => {
      const expDate = new Date().valueOf();
      const optionId = STATE.ASSETS.OPT6CALL.id;

      try {
        /**
         * emit option
         * try to execute option
         * try to execute option one more time
         */

        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        let executeTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'execute',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [
                {
                  amount: STRIKE,
                  assetId: STATE.ASSETS.WUSD.id,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(executeTx.id, {
          apiBase: NODE_URL,
        });

        let execute2Tx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'execute',
                args: [
                  {
                    type: 'string',
                    value: optionId,
                  },
                ],
              },
              payment: [
                {
                  amount: STRIKE,
                  assetId: STATE.ASSETS.WUSD.id,
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(execute2Tx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The option is not active'
        );
        done();
      }
    });
  });
});
