import { execSync } from 'child_process';
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import {
  invokeScript,
  broadcast,
  waitForTx,
  setScript,
} from '@waves/waves-transactions';
import { NODE_URL, STATE, CHAIN_ID } from '../state';
import { getLastBlock, getDayEnd } from '../utils/';
import { DAY_IN_MS, STRIKE, WVS } from '../utils/constants';

describe('dapp optioner', () => {
  describe('emit procedure', () => {
    beforeAll(async () => {
      const path = join('.', 'ride', 'optioner.ride');
      const newFilePath = join('.', 'ride', 'optioner.ride.tmp');
      writeFileSync(
        newFilePath,
        readFileSync(path)
          .toString()
          .replace('PLACERADDRESSHERE', STATE.ACCOUNTS.ALICE.address)
      );
      const script = execSync(`surfboard compile ${newFilePath}`)
        .toString()
        .trim();
      execSync(`rm ${newFilePath}`);

      const tx = await broadcast(
        setScript(
          {
            fee: 0.014 * WVS,
            script: script,
            senderPublicKey: STATE.ACCOUNTS.DAPP.publicKey,
            type: 13,
            timestamp: new Date().valueOf(),
            version: 1,
            chainId: CHAIN_ID,
          },
          STATE.ACCOUNTS.DAPP.seed
        ),
        NODE_URL
      );
      await waitForTx(tx.id, {
        apiBase: NODE_URL,
      });
    });
    /**
     * "emit" procedure
     * - try to emit option with not exists NFT
     * - try to emit option with not NFT asset
     * - try to emit option with STRIKEAmount = 0
     * - try to emit option with not existing STRIKE asset
     * - try to emit option with today expDay
     * - try to emit option without payment
     * - try to emit option with payment.amount = 0
     * - try to emit option with payment.amount not divisible by NFT.quantity
     * - just emit option
     * - try to emit option on already optioned asset
     */
    it('should try to emit option with not exists asset', async done => {
      const expDate = new Date().valueOf() + DAY_IN_MS;

      try {
        const invoke = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: 'SMWRNGSST',
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: 'SMSST',
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(invoke.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The asset does not exists'
        );
        done();
      }
    });

    it('should try to emit option with not NFT asset', async done => {
      const expDate = new Date().valueOf() + DAY_IN_MS;

      try {
        const invoke = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: 'WAVES',
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: 'SMSST',
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(invoke.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: WAVES is not NFT'
        );
        done();
      }
    });

    it('should try to emit option with STRIKE amount = 0', async done => {
      const expDate = new Date().valueOf() + DAY_IN_MS;

      try {
        const invoke = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: STATE.ASSETS.OPT2CALL.id,
                  },
                  {
                    type: 'integer',
                    value: 0,
                  },
                  {
                    type: 'string',
                    value: 'SMSST',
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(invoke.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The option strike amount has to be greater than zero'
        );
        done();
      }
    });

    it('should try to emit option with not existing STRIKE asset', async done => {
      const expDate = new Date().valueOf() + DAY_IN_MS;

      try {
        const invoke = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: STATE.ASSETS.OPT3CALL.id,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: 'SMSST',
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(invoke.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: Strike asset does not exists'
        );
        done();
      }
    });

    it('should try to emit option with invalid expDate', async done => {
      try {
        const lastBlock = await getLastBlock();
        const end = getDayEnd(lastBlock.timestamp);
        const invoke = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: STATE.ASSETS.OPT4CALL.id,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: end,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(invoke.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          'Error while executing account-script: The option expiration date has to be at least the next day'
        );
        done();
      }
    });

    it('should just emit option', async done => {
      const expDate = new Date().valueOf() + DAY_IN_MS;

      try {
        const tx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: STATE.ASSETS.OPT5CALL.id,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(tx.id, {
          apiBase: NODE_URL,
        });

        expect(tx).toHaveProperty('type', 16);
        expect(tx).toHaveProperty(
          'senderPublicKey',
          STATE.ACCOUNTS.ALICE.publicKey
        );
        expect(tx).toHaveProperty('dApp', STATE.ACCOUNTS.DAPP.address);
        expect(tx).toHaveProperty('call', {
          function: 'emit',
          args: [
            {
              type: 'string',
              value: STATE.ASSETS.OPT5CALL.id,
            },
            {
              type: 'integer',
              value: STRIKE,
            },
            {
              type: 'string',
              value: STATE.ASSETS.WUSD.id,
            },
            {
              type: 'integer',
              value: expDate,
            },
          ],
        });
        expect(tx).toHaveProperty('payment', [{ amount: 100, assetId: null }]);
        done();
      } catch (e) {
        done(e);
      }
    });

    it('should try to emit option on already optioned NFT', async done => {
      const expDate = new Date().valueOf() + DAY_IN_MS;

      try {
        const emitTx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: STATE.ASSETS.OPT6CALL.id,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emitTx.id, {
          apiBase: NODE_URL,
        });

        const emit2Tx = await broadcast(
          invokeScript(
            {
              dApp: STATE.ACCOUNTS.DAPP.address,
              call: {
                function: 'emit',
                args: [
                  {
                    type: 'string',
                    value: STATE.ASSETS.OPT6CALL.id,
                  },
                  {
                    type: 'integer',
                    value: STRIKE,
                  },
                  {
                    type: 'string',
                    value: STATE.ASSETS.WUSD.id,
                  },
                  {
                    type: 'integer',
                    value: expDate,
                  },
                ],
              },
              payment: [
                {
                  amount: 100,
                  assetId: 'WAVES',
                },
              ],
              fee: 900000,
              chainId: CHAIN_ID,
            },
            STATE.ACCOUNTS.ALICE.seed
          ),
          NODE_URL
        );

        await waitForTx(emit2Tx.id, {
          apiBase: NODE_URL,
        });

        done('Wrong');
      } catch (e) {
        expect(e).toHaveProperty(
          'message',
          `Error while executing account-script: The option with assetId ${STATE.ASSETS.OPT6CALL.id} already exists`
        );
        done();
      }
    });
  });
});
