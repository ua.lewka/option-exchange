module.exports = {
  preset: 'ts-jest',
  forceExit: true,
  testEnvironment: 'node',
  testTimeout: 5000,
  testMatch: ["**/*.test.js", "**/*.test.ts"],
};