import { NODE_URL } from '@waves/node-state/dist/constants';
import axios from 'axios';
import { DAY_IN_MS, ROOT_DATE } from './constants';

export type Block = {
  blocksize: number;
  features: Array<any>;
  fee: number;
  generator: string;
  height: number;
  reward: number;
  signature: string;
  timestamp: number;
  transactions: Array<any>;
  transactionsCount: number;
  totalFee: number;
  reference: string;
  version: number;
};

export const getLastBlock = () =>
  axios.get<Block>(`${NODE_URL}/blocks/last`).then(res => res.data);

export const getDayStart = (timestamp: number): number =>
  Math.floor((timestamp - ROOT_DATE) / DAY_IN_MS) * DAY_IN_MS + ROOT_DATE;

export const getDayEnd = (timestamp: number): number =>
  getDayStart(timestamp) + DAY_IN_MS - 1;
